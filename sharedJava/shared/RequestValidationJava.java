package shared;

import io.swagger.models.Operation;
import io.swagger.models.Path;
import io.swagger.models.parameters.Parameter;
import io.swagger.parser.SwaggerParser;
import io.swagger.parser.util.SwaggerDeserializationResult;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import models.Request;
import validators.Validator;
import builders.RequestBuilder;

import com.atlassian.oai.validator.report.ValidationReport.Message;
import com.ibm.broker.javacompute.MbJavaComputeNode;
import com.ibm.broker.plugin.MbElement;
import com.ibm.broker.plugin.MbException;
import com.ibm.broker.plugin.MbMessage;
import com.ibm.broker.plugin.MbMessageAssembly;
import com.ibm.broker.plugin.MbOutputTerminal;
import com.ibm.broker.plugin.MbUserException;
import com.ibm.integration.admin.proxy.IntegrationServerProxy;
import com.ibm.integration.admin.proxy.RestApiProxy;

public class RequestValidationJava extends MbJavaComputeNode {

	public void evaluate(MbMessageAssembly inAssembly) throws MbException {
		MbOutputTerminal out = getOutputTerminal("out");
//		MbOutputTerminal alt = getOutputTerminal("alternate");
		MbMessage inMessage = inAssembly.getMessage();
		MbMessageAssembly outAssembly = null;
		try {
			System.out.println("-----------------------");
			System.out.println("Running Validation Test");
			// create new message as a copy of the input
			MbMessage outMessage = new MbMessage(inMessage);
			outAssembly = new MbMessageAssembly(inAssembly, outMessage);
			// ----------------------------------------------------------
			// Add user code below
			
			//get serviceName from user defined variables;
			String serviceName = getUserDefinedAttribute("ServiceName").toString();
			
			// initialize server proxy (defaults to currently running server)
			IntegrationServerProxy server = new IntegrationServerProxy();
			
			// extract RestApiProxy with serviceName
			RestApiProxy api = server.getRestApiByName(serviceName, true);
			// get swagger document from API object
			String document = api.getDocument();

//			get local environment element
			MbMessage inLocalEnvironment = inAssembly.getLocalEnvironment();
			
			//get rest input element
			MbElement restInput = inLocalEnvironment.getRootElement().getFirstElementByPath("/REST/Input");			
			// get method, URI, path, and operation from rest input element
			String method = restInput.getFirstElementByPath("Method").getValueAsString().toLowerCase();
//			String ibmMessageURI = restInput.getFirstElementByPath("URI").getValueAsString();	
			String ibmMessagePath = restInput.getFirstElementByPath("Path").getValueAsString();
	
			// store deserialized swagger
			SwaggerDeserializationResult swagger = new SwaggerParser().readWithInfo(document);
			
			// get swagger path using current message path subtracting the basepath and leaving only the endpoint string
			String messagePath = ibmMessagePath.substring(swagger.getSwagger().getBasePath().length());
			Path path = swagger.getSwagger().getPath(messagePath);
			
			// setup operation swagger object based on current method
			Operation operation = getOperationFromMethod(method, path);
					
			// initialize request builder object
			RequestBuilder requestBuilder = RequestBuilder.getInstance(messagePath, method);
			
			// get rest parameters element
			MbElement restInputParameters = restInput.getFirstElementByPath("Parameters");
			// get first parameter element (will use as iterator)
			MbElement currentParameter = restInputParameters.getFirstChild();

			// instantiate restRequestParamsMap (this is parameter type agnostic)
			Map<String, String> restRequestParamsMap = new HashMap<String, String>();

			// loop through rest params and put in restRequestParamsMap
			while(currentParameter!= null){
				restRequestParamsMap.put(currentParameter.getName(), currentParameter.getValueAsString());		
				currentParameter = currentParameter.getNextSibling();
			}
			
			// get parameters list from documentation operation
			List<Parameter> swaggerParameters = operation.getParameters();

			String body = null;
//			Map<String, String> bodyParameters = new HashMap<>();
			
			// get IBM body element
			MbElement bodyElement = null;
			if(method.equals("post")){
				 MbElement json = inMessage.getRootElement().getFirstElementByPath("JSON");
				 if(json !=null){
					bodyElement = json.getFirstElementByPath("Data");
					body = parseIbmBody(bodyElement);
					
				 }
			}
			
			if(body!= null){
				requestBuilder.setBody(body);
				System.out.println(body);
			}
			
			/**
			 * Loop through parameter from swagger documentation then extract those parameters from the request input parameters.
			 * Then either:
			 * add parameter to request object header
			 * add parameter to request object query params
			 * add parameter to request body map  
			 */
			for(Parameter param: swaggerParameters){
				switch (param.getIn()) {
				case "query":
					requestBuilder.addQueryParam( param.getName(), restRequestParamsMap.get(param.getName()));	
				break;
				case "header":
					requestBuilder.addHeaders(param.getName(), restRequestParamsMap.get(param.getName()));
				break;
				default:
					break;
				}
			}
			
			Request request = requestBuilder.build();
			
			Validator validator = new Validator(document);
			validator.generateReport(request);
			
			StringBuilder errorBuilder = new StringBuilder();

			List<Message> errorsMessages = validator.getErrorsMessages();
			if(errorsMessages != null && errorsMessages.size() >0){
				errorsMessages.forEach(error->{
					errorBuilder.append(" -> "+ error).append(System.lineSeparator());
				});
				String error = "Request Validation Error: " + errorBuilder.toString();
				System.out.println(error);
				
				throw new Exception(error);
			}else{
				System.out.println("Request Validated");
			}
			
			
			

			
		
			// End of user code
			// ----------------------------------------------------------
		} catch (MbException e) {
			// Re-throw to allow Broker handling of MbException
			throw e;
		} catch (RuntimeException e) {
			// Re-throw to allow Broker handling of RuntimeException
			throw e;
		} catch (Exception e) {
			// Consider replacing Exception with type(s) thrown by user code
			// Example handling ensures all exceptions are re-thrown to be handled in the flow
			throw new MbUserException(this, "evaluate()", "", "", e.toString(),
					null);
		}
		// The following should only be changed
		// if not propagating message to the 'out' terminal
		out.propagate(outAssembly);

	}
	
	/**
	 * 
	 * @param bodyMap
	 * @return
	 * 
	 * this is a naive approach and it assumes that the body is only one level deep
	 * 
	 * will need to check how IBM works with multileveled bodies
	 */

	public String parseIbmBody(MbElement body) throws MbException{
		StringBuilder builder = new StringBuilder();
		parseIbmBody(body,builder);
		builder.replace(0, body.getName().length()+3, "");
		return builder.toString();
	}
	
	private void parseIbmBody(MbElement current, StringBuilder builder) throws MbException{

		// check if current node has children
		if(current.getFirstChild()!=null){
			// append node name
			builder.append('"').append(current.getName()).append("\":");

			// if current node has more than one child append opening brackets
			if(! current.getFirstChild().equals(current.getLastChild())){
				builder.append('{');
			}
			
			// extract child node
			MbElement child = current.getFirstChild();
			
			//loop sibling nodes
			while(child != null){
				// check if child has child node
				if(child.getFirstChild()!=null){
					// child has child node so recursive call to parsebody with child node
					parseIbmBody(child, builder);
				}else{
					// child does not have child node so append node value
					builder.append('"').append(child.getName()).append("\":").append('"').append(child.getValue().toString()).append('"');
				}
				
				// assign new sibling element
				child = child.getNextSibling();
				// either close object or add parens for next item
				if(child == null){
					builder.append('}');
				}else{
					builder.append(',');
				}
				
			}
		}
		
	}
		


	
	public Operation getOperationFromMethod(String method, Path path){
		Operation operation = null;
		switch(method){
			case "get":
				operation = path.getGet();
			break;
			case "post":
				operation = path.getPost();
			break;
			case "put":
				operation = path.getPut();
			case "delete":
				operation = path.getDelete();
			break;
			default:
				operation = path.getGet();
			break;
		}
		return operation;
	}

	/**
	 * onPreSetupValidation() is called during the construction of the node
	 * to allow the node configuration to be validated.  Updating the node
	 * configuration or connecting to external resources should be avoided.
	 *
	 * @throws MbException
	 */
	@Override
	public void onPreSetupValidation() throws MbException {
	}

	/**
	 * onSetup() is called during the start of the message flow allowing
	 * configuration to be read/cached, and endpoints to be registered.
	 *
	 * Calling getPolicy() within this method to retrieve a policy links this
	 * node to the policy. If the policy is subsequently redeployed the message
	 * flow will be torn down and reinitialized to it's state prior to the policy
	 * redeploy.
	 *
	 * @throws MbException
	 */
	@Override
	public void onSetup() throws MbException {
	}

	/**
	 * onStart() is called as the message flow is started. The thread pool for
	 * the message flow is running when this method is invoked.
	 *
	 * @throws MbException
	 */
	@Override
	public void onStart() throws MbException {
	}

	/**
	 * onStop() is called as the message flow is stopped. 
	 *
	 * The onStop method is called twice as a message flow is stopped. Initially
	 * with a 'wait' value of false and subsequently with a 'wait' value of true.
	 * Blocking operations should be avoided during the initial call. All thread
	 * pools and external connections should be stopped by the completion of the
	 * second call.
	 *
	 * @throws MbException
	 */
	@Override
	public void onStop(boolean wait) throws MbException {
	}

	/**
	 * onTearDown() is called to allow any cached data to be released and any
	 * endpoints to be deregistered.
	 *
	 * @throws MbException
	 */
	@Override
	public void onTearDown() throws MbException {
	}

}
